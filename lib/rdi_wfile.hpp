#ifndef RDI_WFILE_HPP
#define RDI_WFILE_HPP

#if (__GNUC__ >= 8 || _MSC_VER >= 1910) && __cplusplus >= 2017
#include <filesystem>
#else
#include <experimental/filesystem>
#endif

#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace RDI
{

#if (__GNUC__ >= 8 || _MSC_VER >= 1910) && __cplusplus >= 2017
    namespace fs = std::filesystem;
#else
    namespace fs = std::experimental::filesystem;
#endif

///@brief doning some processing on the file path to support files with long names.
/// https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file#maximum-path-length-limitation

#ifdef _WIN32
std::wstring handle_long_path(std::wstring path);
#endif

std::pair< std::wstring, int> read_file(const std::wstring& file_path);

std::wstring read_wfile(const std::string& filename);

std::vector<std::wstring> read_wfile_lines(const std::string& filename);

std::string read_file(const std::string& filename);

std::vector<std::string> read_file_lines(const std::string& filename);

std::pair< std::vector<char>, int>
read_binary_file(const std::string& file_path);

std::vector<char> read_binary_file(const std::wstring& filename);

#ifdef _WIN32
bool write_wfile(const std::wstring& filename, const std::wstring& fileContent);
#endif

bool write_wfile(const std::string& filename, const std::wstring& fileContent);

bool append_to_wfile(const std::string& filename, const std::wstring& content);

bool write_wfile_lines(const std::string& filename,
                       const std::vector<std::wstring>& linesToWrite);

bool write_file(const std::string& filename, const std::string& fileContent);

int
write_binary_file(const std::string& file_path, const std::vector<char>& content);
int
write_binary_file(const std::wstring& filename, const std::vector<uint8_t>& content);

bool append_to_file(const std::string& filename, const std::string& content);

bool write_file_lines(const std::string& filename,
                      const std::vector<std::string>& linesToWrite);

template <typename T, template <typename...> class Map, typename K, typename V>
bool write_map(const std::string& filename, const Map<K, V>& mapToWrite)
{
    std::basic_ofstream<T> of(filename.c_str());
    if (!of.is_open())
        return false;

    for (std::pair<K, V> line : mapToWrite)
        of << line.first << " " << line.second << std::endl;

    return true;
}

template <typename T, template <typename...> class Map, typename K, typename V>
bool write_map(const std::string& filename,
               const Map<K, std::vector<V>>& mapToWrite)
{
    std::basic_ofstream<T> of(filename.c_str());
    if (!of.is_open())
        return false;

    for (std::pair<K, std::vector<V>> line : mapToWrite)
    {
        of << line.first << std::endl;
        for (V value : line.second)
            of << value << std::endl;
    }
    return true;
}

///  return absolute pathes to files that mathces the predicate
///	 auto files = find_files_that(path); will return all files in that path
///  auto files = find_files_that(asdf,[](const fs::path &p)
///								 {return p.extension() == ".cpp";});
///  will return all files in that path that ends with .cpp
std::vector<std::string>
find_files_that(const fs::path& dir,
                std::function<bool(const fs::path&)> filter
                = [](const fs::path&) { return true; },
                const bool recurse = false, const bool follow_symlinks = false);

bool delete_file(const std::string& path);

std::string get_absolute_path(const std::string&);

/// returns the location of the binary executable ex: "/home/rdi/bin"
std::string get_current_directory();
std::wstring wget_current_directory();

std::vector<std::string> get_directory_content(const std::string& path);

bool create_directory(const std::string& path, bool nested = false);

bool delete_directory(const std::string& path);

bool dump_matrix(const std::string& file_name,
                 std::vector<std::vector<float>>& input_matrix);

bool file_exists(const std::string& filename);
bool file_exists(const std::wstring& filename);
bool directory_exists(const std::string& dir_name);

bool is_directory(const std::string& path);

std::string extract_path_from_filename(const std::string& filename);

std::string extract_filename(const std::string& path);

/// EXAMPLE: "/home/rdi/Documents/" -> "Documents"
std::string extract_last_dirname(const std::string& path);

std::string extract_filename_without_extension(const std::string& path);

std::string extract_extention_from_path(const std::string& filename);

} // namespace RDI

#endif // RDI_WFILE_HPP
