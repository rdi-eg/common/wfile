#include <rdi_stl_utils.hpp>
#include "rdi_wfile.hpp"

#include <sstream>
#include <codecvt>
#include <utility>
#include <algorithm>
#include <memory>

using namespace std;

namespace RDI
{
#define SUCCESS 0
#define FAILURE  -303
#define FILE_OPENING_FAILURE  -444
#define FILE_DOESNOT_EXIST    -3214

#if defined _WIN32
wstring handle_long_path(std::wstring path)
{
    wstring wcurrent_dir = wget_current_directory();

    RDI::replace(path, L'\\', L'/');
    if (path.find(L":/") == string::npos)
    {
        path = wcurrent_dir + L"/" + path;
    }

    // replace evey / with \, because it's necessary
    // for windows to support files with long names
    RDI::replace(path, L'/', L'\\');

    vector<wstring> path_elements = RDI::split(path, L"\\");

    for (size_t i = 0; i < path_elements.size(); i++)
    {
        if (path_elements[i] == L"..")
        {
            path_elements.erase(path_elements.begin() + long(i) - 1,
                                path_elements.begin() + long(i) + 1);
            i -= 2;
        }
    }

    // prepend \\?\ to the file path, because it's necessary for windows to support files with long names.
    // https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file#maximum-path-length-limitation
    wstring out_path = L"\\\\?\\";
    for (size_t i = 0; i < path_elements.size(); i++)
    {
        if (i != path_elements.size() - 1)
        {
            out_path += path_elements[i] + L"\\";
        }
        else
        {
            out_path += path_elements[i];
        }
    }

    return out_path;

}
#endif

std::pair< wstring, int> read_file(const std::wstring& file_path)
{
    #ifdef _WIN32
        wstring file_path__ = handle_long_path(file_path);
    #else
        string file_path__ = wstring_to_string(file_path);
    #endif

    bool bad_path = !fs::exists(file_path__) || fs::is_directory(file_path__);

    if(bad_path) { return {L"", FILE_DOESNOT_EXIST}; }


    wifstream wfhandle(file_path__);

    if(!wfhandle.is_open()) { return {L"", FILE_OPENING_FAILURE}; }
    wfhandle.imbue(locale(wfhandle.getloc(), new codecvt_utf8<wchar_t>));

    wstringstream wss;
    wss << wfhandle.rdbuf();
    return { wss.str(), SUCCESS};
}

wstring read_wfile(const string& filename)
{
    throw_unless(file_exists(filename),
                 std::runtime_error(filename + " file doesn't exist!"));

    wifstream wif(filename.c_str());
    wif.imbue(locale(wif.getloc(), new codecvt_utf8<wchar_t>));
    wstringstream wss;
    wss << wif.rdbuf();
    return wss.str();
}

#ifdef _WIN32
bool write_wfile(const wstring& filename, const wstring& fileContent)
{
    wofstream wof(filename.c_str());

    if(!wof.is_open())
    {
        return false;
    }

    wof.imbue(locale(wof.getloc(), new codecvt_utf8<wchar_t>));
    wof << fileContent;
    return true;
}
#endif

bool write_wfile(const string& filename, const wstring& fileContent)
{
    wofstream wof(filename.c_str());

    if(!wof.is_open())
    {
        return false;
    }

    wof.imbue(locale(wof.getloc(), new codecvt_utf8<wchar_t>));
    wof << fileContent;
    return true;
}

string read_file(const string &filename)
{
    throw_unless(file_exists(filename),
                 std::runtime_error(filename + " file doesn't exist!"));

    ifstream stream(filename.c_str());
    stringstream ss;
    ss << stream.rdbuf();
    return ss.str();
}

pair< vector<char>, int>
read_binary_file(const std::string& file_path)
{
    ifstream fhandle(file_path, ios::binary);
    if(!fhandle.is_open()) { return {{}, FILE_OPENING_FAILURE} ;}

    stringstream ss;
    ss << fhandle.rdbuf();
    string s = ss.str();

    return { vector<char>(s.cbegin(), s.cend()), SUCCESS};
}

std::vector<char>
read_binary_file(const std::wstring& filename)
{
    #ifdef __linux__
        string file_path__ = wstring_to_string(filename);
    #else
        wstring file_path__ = handle_long_path(filename);
    #endif

    ifstream stream (file_path__, std::ios::in | std::ios::binary);
    vector<char> file_bytes;

    stringstream ss;
    ss << stream.rdbuf();
    for(auto c : ss.str())
    {
        file_bytes.emplace_back(c);
    }

    return file_bytes;
}

int
write_binary_file(const std::string& file_path, const std::vector<char>& content)
{
    ofstream fhandle (file_path, ios::binary);
    if(!fhandle.is_open()) { return FILE_OPENING_FAILURE;}

    fhandle.write(content.data(), content.size());
    return SUCCESS;
}

int
write_binary_file(const std::wstring& filename, const std::vector<uint8_t>& content)
{
#ifdef __linux__
    string f = wstring_to_string(filename);
#else
     wstring f = handle_long_path(filename);
#endif

    ofstream stream (f, std::ios::out | std::ios::binary);

    if(!stream.is_open())
    {
        return 1;
    }

    for(auto c : content)
    {
        stream << c;
    }

    return 0;
}

bool write_file(const string &filename, const string &fileContent)
{
    ofstream of(filename.c_str());

    if(!of.is_open())
    {
        return false;
    }

    of << fileContent;
    return true;
}

string get_current_directory()
{
    return fs::current_path().string() + "/";
}

wstring wget_current_directory()
{
    return fs::current_path().wstring() + L"/";
}

std::vector<string> find_files_that(const fs::path& dir,
                                    std::function<bool(const fs::path&)> filter,
                                    const bool recurse,
                                    const bool follow_symlinks)
{
    std::vector<string> result;

    if (! fs::exists(dir) || ! is_directory(dir))
    {
        return result;
    }

    auto dir_ops = follow_symlinks
        ? fs::directory_options::follow_directory_symlink
        : fs::directory_options::none;

    if (recurse)
    {
        for (const auto& entry : fs::recursive_directory_iterator(dir, dir_ops))
        {
            if (fs::is_regular_file(entry) && filter(entry))
            {
                result.push_back(entry.path().string());
            }
        }
    }
    else
    {
        for (const auto& entry : fs::directory_iterator(dir))
        {
            if (fs::is_regular_file(entry) && filter(entry))
            {
                result.push_back(entry.path().string());
            }
        }
    }

    return result;
}

vector<wstring> read_wfile_lines(const string &filename)
{
    throw_unless(file_exists(filename),
                 std::runtime_error(filename + " file doesn't exist!"));

    wifstream wif(filename.c_str());
    wif.imbue(locale(wif.getloc(), new codecvt_utf8<wchar_t>));
    wstringstream wss;
    wss << wif.rdbuf();

    wstring tmp;
    vector<wstring> lines;
    while (getline(wss, tmp))
        lines.push_back(tmp);

    return lines;
}

bool write_wfile_lines(const string &filename,
                       const vector<wstring> &lines_to_write)
{
    wofstream wof(filename.c_str());

    if(!wof.is_open())
    {
        return false;
    }

    wof.imbue(locale(wof.getloc(), new codecvt_utf8<wchar_t>));

    for (const wstring& line : lines_to_write)
    {
        wof << line << endl;
    }

    return true;
}

vector<string> read_file_lines(const string &filename)
{
    throw_unless(file_exists(filename),
                 std::runtime_error(filename + " file doesn't exist!"));

    ifstream stream(filename.c_str());
    stringstream ss;
    ss << stream.rdbuf();

    string tmp;
    vector<string> lines;

    while (getline(ss, tmp))
    {
        lines.push_back(tmp);
    }

    return lines;
}

bool write_file_lines(const string &filename,
                      const vector<string> &lines_to_write)
{
    ofstream of(filename.c_str());

    if(!of.is_open())
    {
        return false;
    }

    for (const string& line : lines_to_write)
    {
        of << line << endl;
    }

    return true;
}

vector<string> get_directory_content(const string& path)
{
    vector<string> output;

    transform(fs::directory_iterator(path),
              fs::directory_iterator(),
              back_inserter(output),
              [](const fs::directory_entry& entry) {
                    return entry.path().filename().string();
              });
    
    // filter `.` and `..` from paths entries
    // This filter is to solve a bug with `Virtualbox`
    //  when sharing folder from linux to windows
	output.erase(std::remove_if(output.begin(),
                                output.end(),
                                [&](const auto &x) {
								    return (x == ".") || (x == "..");
								}),
              	 output.end());

    sort(begin(output), end(output));
    return output;
}

bool append_to_wfile(const string& filename, const wstring& content)
{
    if(!file_exists(filename))
    {
        write_wfile(filename, L"");
    }

    wofstream wof;
    wof.open(filename.c_str(), ios_base::app);

    if(!wof.is_open())
    {
        return false;
    }

    wof.imbue(locale(wof.getloc(), new codecvt_utf8<wchar_t>));
    wof << content << '\n';
    return true;
}

bool append_to_file(const string &filename, const string &content)
{
    if(!file_exists(filename))
    {
        write_file(filename, "");
    }

    ofstream of;
    of.open(filename.c_str(), ios_base::app);

    if(!of.is_open())
    {
        return false;
    }

    of << content << '\n';
    return true;
}

string get_absolute_path(const string& path)
{
    return fs::absolute(path).string();
}

bool create_directory(const string& path, bool nested)
{
    return nested ? fs::create_directories(path):
                    fs::create_directory(path);
}


bool delete_directory(const string& path)
{
    return  fs::remove(path);
}

bool delete_file(const string& path)
{
    if(!file_exists(path))
    {
        return false;
    }

    return fs::remove(path);
}

bool dump_matrix(const std::string &file_name,
                 std::vector<std::vector<float>> &input_matrix)
{
    std::vector<std::string> content;

    for(const auto& inner : input_matrix)
    {
        std::string line;

        for(const auto& item : inner)
        {
            line += std::to_string(item);
            line += " ";
        }

        content.push_back(line);
    }

    return write_file_lines(file_name, content);
}

bool file_exists(const wstring& filename)
{
	if(!fs::exists(filename))
	{
		return false;
	}

	return !fs::is_directory(filename);
}

bool file_exists(const string& filename)
{
    if(!fs::exists(filename))
    {
        return false;
    }

    return !fs::is_directory(filename);
}

bool
directory_exists(const string& dir_name)
{
    if(!fs::exists(dir_name))
    {
        return false;
    }

    return fs::is_directory(dir_name);
}

bool is_directory(const string& path)
{
    return fs::is_directory(path);
}

std::string extract_path_from_filename(const string& filename)
{
    return fs::path(filename).parent_path().string() +'/';
}

std::string extract_filename(const std::string& path)
{
    throw_unless(!is_directory(path),
                 std::runtime_error(path + " is a directory."));

    return fs::path(path).filename().string();
}

std::string extract_last_dirname(const std::string& path)
{
	std::string _path = fs::path(path).filename().string();

	/// NOTE: This is also working using regex 
	///       but it took long time to compile
	//smatch result;
    //regex_search(_path, result, regex("(?:[^/]+)(?=/*$)"));
	//return std::string(result.str());
	
	int start_index = 0;
	int end_index   = _path.size();
	
	while( (end_index > 0) && (_path[--end_index] == '/') );
	start_index = end_index;
	cout << start_index << endl;
	while( (start_index >= 0) && (_path[--start_index] != '/') );
	start_index++;

	return _path.substr(start_index, end_index - start_index + 1);
}

std::string extract_filename_without_extension(const std::string& path)
{
    throw_unless(!is_directory(path),
                 std::runtime_error(path + " is a directory."));

    return fs::path(path).stem().string();
}

std::string extract_extention_from_path(const string& filename)
{
    return fs::path(filename).extension().string();
}

} //namespace RDI
